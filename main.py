import datetime
import numpy as np
import pandas as pd
import sklearn

from pandas.io.data import DataReader
from sklearn.linear_model import LogisticRegression
from sklearn.lda import LDA
from sklearn.qda import QDA

from csv_reader import QuantQuoteReader, SymbolsList
from plotter import Plotter


def main():
    symbols = SymbolsList().read_SP100()

    reader = QuantQuoteReader(symbol='AAPL')
    reader.run(fill_dates=False)
    # reader.fill_empty()
    plot = Plotter(reader.data)

if __name__ == '__main__':
    main()
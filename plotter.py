import matplotlib.pyplot as plt


class Plotter(object):
    """

        Plot a given time series represented in a DataFrame

    """

    def __init__(self, data):
        self.data = data
        self.data.plot()
        plt.show()

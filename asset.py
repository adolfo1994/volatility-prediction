import datetime
import pandas as pd
from csv_reader import QuantQuoteReader
import pandas.io.data as web


class Asset(object):

    def __init__(self, symbol, source):
        self.symbol = symbol
        if source == 'yahoo':
            start = datetime.datetime(year=1900, month=1, day=1)
            end = datetime.datetime.now()
            df = web.DataReader(self.symbol, 'yahoo', start, end)
            self.historical_data = pd.DataFrame(df['Close'])
        elif source == "quantquote":
            reader = QuantQuoteReader(symbol=self.symbol)
            reader.run()
            self.historical_data = pd.DataFrame(reader.data['Close'])

    def calc_returns(self):
        for date in self.historical_data:
            self.historical_data.ix[date]['Return'] = (
                self.historical_data[date - 1] / self.historical_data[date] - 1
            )

    def calc_stdev(self):
        # use Exponential Moving average to calculate windowed standard deviation

    def calc_annualized_volatility(self):
        # multiply by annualization factor based on data period (daily)

def main():
    a = Asset('AAPL', 'yahoo')
    a.calc_returns()
    print a.historical_data

if __name__ == '__main__':
    main()
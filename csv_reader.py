import csv
from fancyimpute import BiScaler, KNN, NuclearNormMinimization, SoftImpute,SimpleFill, MICE
import pandas as pd
import numpy as np


class SymbolsList(object):

    def read_SP500(self):
        self.data = []
        with open("constituents.csv", 'r') as f:
            f.readline()
            reader = csv.reader(f)
            for row in reader:
                self.data.append(
                    {
                        'symbol': row[0],
                        'company': row[1],
                    }
                )

    def read_SP100(self):
        self.data = []
        with open("companies.csv", "r") as f:
            reader = csv.reader(f)
            for row in reader:
                self.data.append({"symbol": row[0], "name": row[1]})
        return self.data


class QuantQuoteReader(object):
    """
    Read data from a QuantQuote historical data csv

    :param symbol: The stock symbol whose csv will be read

    """
    folder_name = "quantquote_daily_sp500/daily/"

    def __init__(self, symbol):
        self.symbol = symbol.lower()
        print "Symbol: ", self.symbol
        self.filename = "%stable_%s.csv" % (self.folder_name, self.symbol)
        print "Filename: ", self.filename
        self.data = {}

    def run(self, fill_dates=True):
        with open(self.filename, 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                self.process_row(row)
        self.data = pd.DataFrame(self.data).transpose()
        self.data = self.data.astype(float)
        self.data.index = pd.DatetimeIndex(self.data.index)
        # if fill_dates:
        #     self.start = self.data.index[0]
        #     self.end = self.data.index[-1]
        #     all_dates = pd.date_range(start=self.start, end=self.end)
        #     self.data = self.data.reindex(all_dates, fill_value=None)
        #     print self.data.describe()
        #     print "Missing: ", sum(self.data['close'].isnull())

    def process_row(self, row):
        year = row[0][:4]
        month = row[0][4:6]
        day = row[0][6:]
        data = dict(
            Open=row[2],
            High=row[3],
            Low=row[4],
            Close=row[5],
        )
        self.data["%s-%s-%s" % (year, month, day)] = data

    # def fill_empty(self):
    #     all_dates = pd.date_range(start=self.start, end=self.end)
    #     print self.data['close']
    #     self.data = pd.DataFrame(
    #         SimpleFill().complete(
    #         self.data),
    #         index=all_dates,
    #         columns=['open', 'high', 'low', 'close']
    #     )
